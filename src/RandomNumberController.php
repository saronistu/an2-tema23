<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class RandomNumberController extends AbstractController
{
    /**
     * @Route("/random", name="index")
     */
    public function index()
    {
        return $this->render('random/index.html.twig', [
            'controller_name' => 'RandomNumberController',
        ]);
    }

    /**
     * @Route("/random/{text}", name="random")
     */
    public function randomNumber($text) {
        $number = random_int(0, 100);
        if ($text < $number) {
            return new Response('<html><body style="font-size:50px;">'."PREA MIC".'</body></html>');
        } else if ($text > $number) {
        return new Response('<html><body style="font-size:50px;">'."PREA MARE".'</body></html>');
    } else {
        return new Response('<html><body style="font-size:50px;">'."EXACT".'</body></html>');
    }
}
}
